package net.guerlab.sdk.wx.helper;

import java.io.StringReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.WeiXinException;
import net.guerlab.sdk.wx.entity.Coupon;
import net.guerlab.sdk.wx.entity.UnifiedOrderNotifyData;

/**
 * 统一订单异步通知结果助手
 *
 * @author guer
 *
 */
public class UnifiedOrderNotifyDataHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnifiedOrderNotifyDataHelper.class);

    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    private UnifiedOrderNotifyDataHelper() {

    }

    /**
     * 解析统一订单异步通知结果
     *
     * @param body
     *            xml内容
     * @return 统一订单异步通知结果
     */
    public static UnifiedOrderNotifyData parse(
            String body) {

        LOGGER.debug("document body : {}", body);

        if (StringUtils.isBlank(body)) {
            return null;
        }

        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(new StringReader(body));

            return parse(document);
        } catch (Exception e) {
            throw new WeiXinException(e.getMessage(), e);
        }
    }

    /**
     * 解析统一订单异步通知结果
     *
     * @param document
     *            xml文档对象
     * @return 统一订单异步通知结果
     */
    public static UnifiedOrderNotifyData parse(
            Document document) {

        LOGGER.debug("document : {}", document == null ? null : document.getText());

        if (document == null) {
            return null;
        }

        Element root = document.getRootElement();

        errorCheck(root);

        return parseData(root);
    }

    private static UnifiedOrderNotifyData parseData(
            Element root) {
        UnifiedOrderNotifyData data = new UnifiedOrderNotifyData();

        data.setAppId(root.elementText("appid"));
        data.setAttach(root.elementText("attach"));
        data.setBankType(root.elementText("bank_type"));

        String cashFee = root.elementText("cash_fee");

        if (StringUtils.isNoneBlank(cashFee)) {
            try {
                data.setCashFee(new BigDecimal(cashFee).divide(HUNDRED));
            } catch (Exception e) {
                LOGGER.debug(e.getMessage(), e);
            }
        }

        data.setCashFeeType(root.elementText("cash_fee_type"));
        try {
            Integer couponCount = Integer.parseInt(root.elementText("coupon_count"));
            data.setCouponCount(couponCount);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }

        String couponFee = root.elementText("coupon_fee");

        if (StringUtils.isNoneBlank(couponFee)) {
            try {
                data.setCouponFee(new BigDecimal(couponFee).divide(HUNDRED));
            } catch (Exception e) {
                LOGGER.debug(e.getMessage(), e);
            }
        }

        data.setDeviceInfo(root.elementText("device_info"));
        data.setFeeType(root.elementText("fee_type"));
        data.setMchId(root.elementText("mch_id"));
        data.setNonceStr(root.elementText("nonce_str"));
        data.setOpenId(root.elementText("openid"));
        data.setOutTradeNo(root.elementText("out_trade_no"));

        String settlementTotalFee = root.elementText("settlement_total_fee");

        if (StringUtils.isNoneBlank(settlementTotalFee)) {
            try {
                data.setSettlementTotalFee(new BigDecimal(settlementTotalFee).divide(HUNDRED));
            } catch (Exception e) {
                LOGGER.debug(e.getMessage(), e);
            }
        }
        data.setSign(root.elementText("sign"));
        data.setSignType(root.elementText("sign_type"));
        data.setSubscribe("Y".equals(root.elementText("is_subscribe")));
        data.setTimeEnd(LocalDateTime.parse(root.elementText("time_end"), FORMATTER));

        String totalFee = root.elementText("total_fee");

        if (StringUtils.isNoneBlank(totalFee)) {
            try {
                data.setTotalFee(new BigDecimal(totalFee).divide(HUNDRED));
            } catch (Exception e) {
                LOGGER.debug(e.getMessage(), e);
            }
        }

        data.setTradeType(root.elementText("trade_type"));
        data.setTransactionId(root.elementText("transaction_id"));

        parseCoupon(root, data);

        return data;
    }

    private static void parseCoupon(
            Element root,
            UnifiedOrderNotifyData data) {
        int count = data.getCouponCount();
        for (int i = 0; i < count; i++) {
            String type = root.elementText("coupon_type_" + i);
            String id = root.elementText("coupon_id_" + i);
            String feeStr = root.elementText("coupon_fee_" + i);

            if (StringUtils.isNoneBlank(feeStr)) {
                try {
                    BigDecimal fee = new BigDecimal(feeStr).divide(HUNDRED);

                    Coupon coupon = new Coupon(id, type, fee);

                    data.getCouponList().add(coupon);
                } catch (Exception e) {
                    LOGGER.debug(e.getMessage(), e);
                }
            }
        }
    }

    private static void errorCheck(
            Element root) {
        String returnCode = root.elementTextTrim("return_code");
        String resultCode = root.elementTextTrim("result_code");

        if (WeiXinConstants.FAIL.equals(returnCode)) {
            throw new WeiXinException(root.elementTextTrim("return_msg"));
        } else if (WeiXinConstants.FAIL.equals(resultCode)) {
            throw new WeiXinException(root.elementTextTrim("err_code") + ", " + root.elementTextTrim("err_code_des"));
        }
    }
}
