package net.guerlab.sdk.wx.response.oauth;

import net.guerlab.sdk.wx.response.AbstractResponse;

/**
 * 抽象Oauth响应
 * 
 * @author guer
 *
 * @param <T>
 *            响应数据类型
 */
public abstract class AbstractOauthResponse<T> extends AbstractResponse<T> {
}
