package net.guerlab.sdk.wx.request.oauth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.response.oauth.AccessTokenData;
import net.guerlab.sdk.wx.response.oauth.AccessTokenResponse;

/**
 * AccessToken请求
 * 
 * @author guer
 *
 */
public class AccessTokenRequest extends AbstractOauthRequest<AccessTokenResponse, AccessTokenData> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccessTokenRequest.class);

    @Override
    protected StringBuilder createRequestUrl(
            String appId,
            String appKey,
            String payAppid,
            String paySecret,
            String payMchId,
            String payKey) {
        StringBuilder builder = new StringBuilder(WeiXinConstants.ACCESS_TOKEN_URL);
        builder.append("?grant_type=authorization_code&appid=");
        builder.append(appId);
        builder.append("&secret=");
        builder.append(appKey);
        return builder;
    }

    @Override
    protected void execut0(
            String responseData) {
        LOGGER.debug("get response data[{}]", responseData);

        AccessTokenData accessTokenData = parseResponseJsData(responseData, AccessTokenData.class);

        response = new AccessTokenResponse();
        response.setData(accessTokenData);
    }

    /**
     * 设置code
     * 
     * @param code
     *            code
     */
    public void setCode(
            String code) {
        requestParams.put("code", code);
    }

    /**
     * 获取code
     * 
     * @return code
     */
    public String getCode() {
        return requestParams.get("code");
    }
}
