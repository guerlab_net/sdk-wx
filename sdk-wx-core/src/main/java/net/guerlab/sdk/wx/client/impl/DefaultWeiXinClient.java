package net.guerlab.sdk.wx.client.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.WeiXinException;
import net.guerlab.sdk.wx.client.AbstractWeiXinClient;
import net.guerlab.sdk.wx.request.AbstractRequest;
import net.guerlab.sdk.wx.request.oauth.AbstractOauthRequest;
import net.guerlab.sdk.wx.request.pay.AbstractPayRequest;
import net.guerlab.sdk.wx.response.AbstractResponse;
import net.guerlab.sdk.wx.response.oauth.AbstractOauthResponse;
import net.guerlab.sdk.wx.response.pay.AbstractPayResponse;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * 默认微信请求客户端
 * 
 * @author guer
 *
 */
public class DefaultWeiXinClient extends AbstractWeiXinClient {

    /**
     * http请求客户端
     */
    private OkHttpClient client;

    /**
     * objectMapper
     */
    private ObjectMapper objectMapper;

    /**
     * 构造微信请求客户端
     * 
     * @param appId
     *            应用ID
     * @param appKey
     *            应用Key
     * @param payAppid
     *            支付应用ID
     * @param paySecret
     *            支付应用Key
     * @param payMchId
     *            商户ID
     * @param payKey
     *            商户Key
     * @param client
     *            http请求客户端
     * @param objectMapper
     *            objectMapper
     */
    public DefaultWeiXinClient(
            String appId,
            String appKey,
            String payAppid,
            String paySecret,
            String payMchId,
            String payKey,
            OkHttpClient client,
            ObjectMapper objectMapper) {
        super(appId, appKey, payAppid, paySecret, payMchId, payKey);
        this.client = client;
        this.objectMapper = objectMapper;
    }

    @Override
    public <T extends AbstractOauthResponse<E>, E> T execute(
            AbstractOauthRequest<T, E> request) {
        request.setObjectMapper(objectMapper);
        String uri = request.getRequestUri(appId, appKey, payAppid, paySecret, payMchId, payKey);
        return execute0(request, uri);
    }

    @Override
    public <T extends AbstractPayResponse<E>, E> T execute(
            AbstractPayRequest<T, E> request) {
        request.setObjectMapper(objectMapper);
        String uri = request.getRequestUri(payAppid, payKey, payMchId, paySecret);
        return execute0(request, uri);
    }

    private <T extends AbstractResponse<E>, E> T execute0(
            AbstractRequest<T, E> request,
            String uri) {
        if (!request.needHttpRequest()) {
            return request.execute(uri).getResponse();
        }

        return executeWithHttpRequest(request, uri);
    }

    private <T extends AbstractResponse<E>, E> T executeWithHttpRequest(
            AbstractRequest<T, E> request,
            String uri) {
        Builder builder = new Request.Builder();
        builder.url(uri);

        MediaType mediaType = MediaType.parse(getContentType(request));

        setContent(request, builder, mediaType);

        return request.execute(getHttpResponceString(builder)).getResponse();
    }

    private <T extends AbstractResponse<E>, E> String getContentType(
            AbstractRequest<T, E> request) {
        return WeiXinConstants.FORMAT_XML.equals(request.getFormat()) ?
                WeiXinConstants.CONTENT_TYPE_XML :
                WeiXinConstants.CONTENT_TYPE_JSON;
    }

    private <T extends AbstractResponse<E>, E> void setContent(
            AbstractRequest<T, E> request,
            Builder builder,
            MediaType mediaType) {
        String content = request.getRequestContent();

        if (content == null) {
            return;
        }

        if (WeiXinConstants.METHOD_POST.equals(request.getMethod())) {
            builder.post(RequestBody.create(mediaType, content));
        } else if (WeiXinConstants.METHOD_PUT.equals(request.getMethod())) {
            builder.put(RequestBody.create(mediaType, content));
        } else if (WeiXinConstants.METHOD_DELETE.equals(request.getMethod())) {
            builder.delete(RequestBody.create(mediaType, content));
        }
    }

    private String getHttpResponceString(
            Builder builder) {
        Call call = client.newCall(builder.build());
        try {
            Response response = call.execute();
            return response.body().string();
        } catch (Exception e) {
            throw new WeiXinException(e.getMessage(), e);
        }
    }

    /**
     * 返回 http请求客户端
     *
     * @return http请求客户端
     */
    public OkHttpClient getClient() {
        return client;
    }

    /**
     * 设置http请求客户端
     *
     * @param client
     *            http请求客户端
     */
    public void setClient(
            OkHttpClient client) {
        this.client = client;
    }

    /**
     * 返回 objectMapper
     *
     * @return objectMapper
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * 设置objectMapper
     *
     * @param objectMapper
     *            objectMapper
     */
    public void setObjectMapper(
            ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
