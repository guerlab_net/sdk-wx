package net.guerlab.sdk.wx.request.pay;

import java.util.Map.Entry;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import net.guerlab.sdk.wx.request.AbstractRequest;
import net.guerlab.sdk.wx.response.pay.AbstractPayResponse;

/**
 * 抽象支付请求
 * 
 * @author guer
 *
 * @param <T>
 *            响应类型
 * @param <E>
 *            响应数据类型
 */
public abstract class AbstractPayRequest<T extends AbstractPayResponse<E>, E> extends AbstractRequest<T, E> {

    /**
     * 构造基本请求地址
     * 
     * @param payAppid
     *            支付应用ID
     * @param payKey
     *            支付应用key
     * @param payMchId
     *            商户ID
     * @param paySecret
     *            商户秘钥
     * @return 请求地址
     */
    protected abstract StringBuilder createRequestUrl(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret);

    /**
     * 获取md5待签名结果
     * 
     * @return md5待签名结果
     */
    protected abstract String getStringA();

    /**
     * 获取md5签名结果
     * 
     * @return md5签名结果
     */
    protected String getMD5Sign() {
        String stringA = getStringA();
        return DigestUtils.md5Hex(stringA);
    }

    /**
     * 获取请求地址
     * 
     * @param payAppid
     *            支付应用ID
     * @param payKey
     *            支付应用key
     * @param payMchId
     *            商户ID
     * @param paySecret
     *            商户秘钥
     * @return 请求地址
     */
    public String getRequestUri(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        StringBuilder builder = createRequestUrl(payAppid, payKey, payMchId, paySecret);

        if (requestParams.isEmpty()) {
            return builder.toString();
        }

        for (Entry<String, String> entry : requestParams.entrySet()) {
            if (StringUtils.isBlank(entry.getKey())) {
                continue;
            }
            if (builder.indexOf("?") != -1) {
                builder.append("&");
            } else {
                builder.append("?");
            }

            builder.append(entry.getKey());
            builder.append("=");
            builder.append(getRequestValue(entry.getValue()));
        }

        return builder.toString();
    }
}
