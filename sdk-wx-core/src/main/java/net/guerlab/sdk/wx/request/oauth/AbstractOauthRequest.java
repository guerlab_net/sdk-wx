package net.guerlab.sdk.wx.request.oauth;

import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import net.guerlab.sdk.wx.request.AbstractRequest;
import net.guerlab.sdk.wx.response.oauth.AbstractOauthResponse;

/**
 * 抽象Oauth请求
 * 
 * @author guer
 *
 * @param <T>
 *            响应类型
 * @param <E>
 *            响应数据类型
 */
public abstract class AbstractOauthRequest<T extends AbstractOauthResponse<E>, E> extends AbstractRequest<T, E> {

    /**
     * 构造基本请求地址
     * 
     * @param appId
     *            应用ID
     * @param appKey
     *            应用key
     * @param payAppid
     *            支付应用ID
     * @param payKey
     *            支付应用key
     * @param payMchId
     *            商户ID
     * @param paySecret
     *            商户秘钥
     * @return 请求地址
     */
    protected abstract StringBuilder createRequestUrl(
            String appId,
            String appKey,
            String payAppid,
            String paySecret,
            String payMchId,
            String payKey);

    /**
     * 获取基本请求地址
     * 
     * @param appId
     *            应用ID
     * @param appKey
     *            应用key
     * @param payAppid
     *            支付应用ID
     * @param payKey
     *            支付应用key
     * @param payMchId
     *            商户ID
     * @param paySecret
     *            商户秘钥
     * @return 请求地址
     */
    public String getRequestUri(
            String appId,
            String appKey,
            String payAppid,
            String paySecret,
            String payMchId,
            String payKey) {
        StringBuilder builder = createRequestUrl(appId, appKey, payAppid, paySecret, payMchId, payKey);

        if (requestParams.isEmpty()) {
            return builder.toString();
        }

        for (Entry<String, String> entry : requestParams.entrySet()) {
            if (StringUtils.isBlank(entry.getKey())) {
                continue;
            }
            if (builder.indexOf("?") != -1) {
                builder.append("&");
            } else {
                builder.append("?");
            }

            builder.append(entry.getKey());
            builder.append("=");
            builder.append(getRequestValue(entry.getValue()));
        }

        return builder.toString();
    }
}
