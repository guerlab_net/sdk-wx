package net.guerlab.sdk.wx.response.oauth;

/**
 * AccessToken响应
 * 
 * @author guer
 *
 */
public class AccessTokenResponse extends AbstractOauthResponse<AccessTokenData> {

}
