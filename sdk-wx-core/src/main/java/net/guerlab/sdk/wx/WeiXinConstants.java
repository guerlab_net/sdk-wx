package net.guerlab.sdk.wx;

/**
 * 微信常量
 * 
 * @author guer
 *
 */
public abstract class WeiXinConstants {

    /**
     * 成功
     */
    public static final String SUCCESS = "SUCCESS";

    /**
     * 失败
     */
    public static final String FAIL = "FAIL";

    /**
     * GET请求
     */
    public static final String METHOD_GET = "GET";

    /**
     * POST请求
     */
    public static final String METHOD_POST = "POST";

    /**
     * PUT请求
     */
    public static final String METHOD_PUT = "PUT";

    /**
     * DELETE请求
     */
    public static final String METHOD_DELETE = "DELETE";

    /**
     * 默认scope
     */
    public static final String DEFAULT_SCOPE = "snsapi_base,snsapi_userinfo";

    /**
     * 版本号
     */
    public static final String VERSION = "2.0.0.0";

    /**
     * QRCONNECT_URL
     */
    public static final String QRCONNECT_URL = "https://open.weixin.qq.com/connect/qrconnect";

    /**
     * AUTHORIZE_URL
     */
    public static final String AUTHORIZE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize";

    /**
     * ACCESS_TOKEN_URL
     */
    public static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";

    /**
     * 统一下单请求地址
     */
    public static final String UNIFIEDORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    /**
     * 退款请求地址
     */
    public static final String REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";

    /**
     * UTF-8编码格式
     */
    public static final String CHARSET_UTF8 = "UTF-8";

    /**
     * JSON格式化
     */
    public static final String FORMAT_JSON = "json";

    /**
     * XML格式化
     */
    public static final String FORMAT_XML = "xml";

    /**
     * JSON请求数据类型
     */
    public static final String CONTENT_TYPE_JSON = "application/json";

    /**
     * xml请求数据类型
     */
    public static final String CONTENT_TYPE_XML = "text/xml";

    /**
     * jsapi交易类型
     */
    public static final String TRADE_TYPE_JSAPI = "JSAPI";

    /**
     * native交易类型
     */
    public static final String TRADE_TYPE_NATIVE = "NATIVE";

    /**
     * app交易类型
     */
    public static final String TRADE_TYPE_APP = "APP";

    /**
     * web交易类型
     */
    public static final String TRADE_TYPE_MWEB = "MWEB";

    private WeiXinConstants() {
        /**
         * not to do some thing
         */
    }

}
