package net.guerlab.sdk.wx.client;

import org.apache.commons.lang3.StringUtils;

/**
 * 抽象微信请求客户端
 * 
 * @author guer
 *
 */
public abstract class AbstractWeiXinClient implements WeiXinClient {

    /**
     * 应用ID
     */
    protected String appId;

    /**
     * 应用Key
     */
    protected String appKey;

    /**
     * 支付应用ID
     */
    protected String payAppid;

    /**
     * 支付应用Key
     */
    protected String paySecret;

    /**
     * 商户ID
     */
    protected String payMchId;

    /**
     * 商户Key
     */
    protected String payKey;

    /**
     * 构造微信请求客户端
     * 
     * @param appId
     *            应用ID
     * @param appKey
     *            应用Key
     * @param payAppid
     *            支付应用ID
     * @param paySecret
     *            支付应用Key
     * @param payMchId
     *            商户ID
     * @param payKey
     *            商户Key
     */
    public AbstractWeiXinClient(
            String appId,
            String appKey,
            String payAppid,
            String paySecret,
            String payMchId,
            String payKey) {
        this.appId = appId;
        this.appKey = appKey;
        this.payAppid = StringUtils.isBlank(payAppid) ? appId : payAppid;
        this.paySecret = StringUtils.isBlank(paySecret) ? appId : paySecret;
        this.payMchId = payMchId;
        this.payKey = payKey;
    }
}
