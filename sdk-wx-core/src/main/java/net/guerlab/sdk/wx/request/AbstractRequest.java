package net.guerlab.sdk.wx.request;

import java.net.URLEncoder;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.WeiXinException;
import net.guerlab.sdk.wx.entity.ErrorData;
import net.guerlab.sdk.wx.response.AbstractResponse;

/**
 * 抽象请求
 *
 * @author guer
 *
 * @param <T>
 *            响应类型
 * @param <E>
 *            响应数据类型
 */
public abstract class AbstractRequest<T extends AbstractResponse<E>, E> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRequest.class);

    protected final Map<String, String> requestParams = new TreeMap<>();

    private ObjectMapper objectMapper;

    protected T response;

    /**
     * 获取是否需要进行http请求
     *
     * @return 是否需要进行http请求
     */
    public boolean needHttpRequest() {
        return true;
    }

    /**
     * 获取请求方式
     *
     * @return 请求方式
     */
    public String getMethod() {
        return WeiXinConstants.METHOD_POST;
    }

    /**
     * 获取数据格式
     *
     * @return 数据格式
     */
    public String getFormat() {
        return WeiXinConstants.FORMAT_JSON;
    }

    /**
     * 获取请求正文
     *
     * @return 请求正文
     */
    public String getRequestContent() {
        return null;
    }

    /**
     * 执行请求
     *
     * @param responseData
     *            响应数据
     */
    protected abstract void execut0(
            String responseData);

    /**
     * 执行请求
     *
     * @param responseData
     *            响应数据
     * @return 响应实体
     */
    public AbstractRequest<T, E> execute(
            String responseData) {
        execut0(responseData);
        return this;
    }

    /**
     * 解析响应js数据
     *
     * @param responseData
     *            响应数据
     * @param clazz
     *            json实体类型
     * @param <D>
     *            json实体类型
     * @return json数据
     */
    public final <D> D parseResponseJsData(
            String responseData,
            Class<D> clazz) {
        ObjectMapper mapper = objectMapper;
        if (mapper == null) {
            mapper = new ObjectMapper();
        }

        try {
            ErrorData errorData = mapper.readValue(responseData, ErrorData.class);

            if (StringUtils.isNotBlank(errorData.getErrcode())) {
                throw new WeiXinException("code : " + errorData.getErrcode() + ", msg : " + errorData.getErrmsg());
            }

            D data = mapper.readValue(responseData, clazz);

            return data;
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            throw new WeiXinException(e.getMessage(), e);
        }
    }

    /**
     * 获取请求值
     *
     * @param object
     *            数据对象
     * @return 字符串值
     */
    protected final static String getRequestValue(
            Object object) {
        if (object == null) {
            return "";
        }

        try {
            return URLEncoder.encode(object.toString(), WeiXinConstants.CHARSET_UTF8);
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            return "";
        }
    }

    /**
     * 获取响应实体
     *
     * @return 响应实体
     */
    public final T getResponse() {
        return response;
    }

    /**
     * 返回 objectMapper
     *
     * @return objectMapper
     */
    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * 设置objectMapper
     *
     * @param objectMapper
     *            objectMapper
     */
    public void setObjectMapper(
            ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
