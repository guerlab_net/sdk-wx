package net.guerlab.sdk.wx.response.pay;

/**
 * 统一支付响应
 *
 * @author guer
 *
 */
public class UnifiedOrderResponse extends AbstractPayResponse<UnifiedOrder> {
}
