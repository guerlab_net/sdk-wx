package net.guerlab.sdk.wx.request.pay;

import java.util.Map.Entry;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.helper.RandomStringHelper;
import net.guerlab.sdk.wx.response.pay.InnerH5Params;
import net.guerlab.sdk.wx.response.pay.InnerH5Response;

/**
 * 统一支付请求
 *
 * @author guer
 *
 */
public class InnerH5Request extends AbstractPayRequest<InnerH5Response, InnerH5Params> {

    private String key;

    private String prepayId;

    @Override
    public boolean needHttpRequest() {
        return false;
    }

    @Override
    protected StringBuilder createRequestUrl(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        key = payKey;
        requestParams.put("appId", payAppid);
        requestParams.put("paySign", getMD5Sign());

        return new StringBuilder(WeiXinConstants.UNIFIEDORDER_URL);
    }

    @Override
    protected void execut0(
            String responseData) {
        InnerH5Params appPayParams = new InnerH5Params();
        appPayParams.setAppId(requestParams.get("appId"));
        appPayParams.setTimeStamp(requestParams.get("timeStamp"));
        appPayParams.setNonceStr(requestParams.get("nonceStr"));
        appPayParams.setPackages(requestParams.get("package"));
        appPayParams.setSignType(requestParams.get("signType"));
        appPayParams.setPaySign(requestParams.get("paySign"));

        response = new InnerH5Response();
        response.setData(appPayParams);
    }

    @Override
    protected String getStringA() {
        String nonceStr = RandomStringHelper.nextString(32);
        String timeStamp = Long.toString(System.currentTimeMillis() / 1000);

        requestParams.put("timeStamp", timeStamp);
        requestParams.put("nonceStr", nonceStr);
        requestParams.put("signType", "MD5");

        StringBuilder builder = new StringBuilder();

        for (Entry<String, String> entry : requestParams.entrySet()) {
            builder.append(entry.getKey());
            builder.append("=");
            builder.append(entry.getValue());
            builder.append("&");
        }

        builder.append("key=");
        builder.append(key);

        return builder.toString();
    }

    /**
     * 获取预支付ID
     *
     * @return 预支付ID
     */
    public String getPrepayId() {
        return prepayId;
    }

    /**
     * 设置预支付ID
     *
     * @param prepayId
     *            预支付ID
     */
    public void setPrepayId(
            String prepayId) {
        this.prepayId = prepayId;
        requestParams.put("package", "prepay_id=" + prepayId);
    }

}
