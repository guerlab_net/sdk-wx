package net.guerlab.sdk.wx.response;

/**
 * 抽象响应对象
 * 
 * @author guer
 *
 * @param <T>
 *            响应数据类型
 */
public abstract class AbstractResponse<T> {

    /**
     * 数据
     */
    private T data;

    /**
     * 获取数据
     * 
     * @return 数据
     */
    public final T getData() {
        return data;
    }

    /**
     * 设置数据
     * 
     * @param data
     *            数据
     */
    public final void setData(
            T data) {
        this.data = data;
    }
}
