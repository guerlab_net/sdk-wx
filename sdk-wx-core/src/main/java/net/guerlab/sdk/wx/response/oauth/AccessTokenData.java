package net.guerlab.sdk.wx.response.oauth;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * access_token数据
 * 
 * @author guer
 *
 */
public class AccessTokenData {

    /**
     * access_token
     */
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * expires_in
     */
    @JsonProperty("expires_in")
    private long expiresIn;

    /**
     * refresh_token
     */
    @JsonProperty("refresh_token")
    private String refreshToken;

    /**
     * openid
     */
    @JsonProperty("openid")
    private String openId;

    /**
     * unionid
     */
    @JsonProperty("unionid")
    private String unionId;

    /**
     * 返回 accessToken
     *
     * @return accessToken
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * 设置accessToken
     *
     * @param accessToken
     *            accessToken
     */
    public void setAccessToken(
            String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 返回 expiresIn
     *
     * @return expiresIn
     */
    public long getExpiresIn() {
        return expiresIn;
    }

    /**
     * 设置expiresIn
     *
     * @param expiresIn
     *            expiresIn
     */
    public void setExpiresIn(
            long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * 返回 refreshToken
     *
     * @return refreshToken
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * 设置refreshToken
     *
     * @param refreshToken
     *            refreshToken
     */
    public void setRefreshToken(
            String refreshToken) {
        this.refreshToken = refreshToken;
    }

    /**
     * 返回 openId
     *
     * @return openId
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * 设置openId
     *
     * @param openId
     *            openId
     */
    public void setOpenId(
            String openId) {
        this.openId = openId;
    }

    /**
     * 返回 unionId
     *
     * @return unionId
     */
    public String getUnionId() {
        return unionId;
    }

    /**
     * 设置unionId
     *
     * @param unionId
     *            unionId
     */
    public void setUnionId(
            String unionId) {
        this.unionId = unionId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AccessToken [accessToken=");
        builder.append(accessToken);
        builder.append(", expiresIn=");
        builder.append(expiresIn);
        builder.append(", refreshToken=");
        builder.append(refreshToken);
        builder.append(", openId=");
        builder.append(openId);
        builder.append(", unionId=");
        builder.append(unionId);
        builder.append("]");
        return builder.toString();
    }
}
