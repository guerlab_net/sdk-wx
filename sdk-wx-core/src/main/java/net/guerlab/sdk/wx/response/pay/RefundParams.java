package net.guerlab.sdk.wx.response.pay;

import io.swagger.annotations.ApiModel;

/**
 * 退款参数
 *
 * @author guer
 *
 */
@ApiModel("退款参数")
public class RefundParams {

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 商户号
     */
    private String mchId;

    /**
     * 随机字符串
     */
    private String nonceStr;

    /**
     * 签名
     */
    private String sign;

    /**
     * 微信订单号
     */
    private String transactionId;

    /**
     * 商户订单号
     */
    private String outTradeNo;

    /**
     * 商户退款单号
     */
    private String outRefundNo;

    /**
     * 微信退款单号
     */
    private String refundId;

    /**
     * 退款金额
     */
    private String refundFee;

    /**
     * 返回 应用ID
     *
     * @return 应用ID
     */
    public final String getAppId() {
        return appId;
    }

    /**
     * 设置应用ID
     *
     * @param appId
     *            应用ID
     */
    public final void setAppId(
            String appId) {
        this.appId = appId;
    }

    /**
     * 返回 商户号
     *
     * @return 商户号
     */
    public final String getMchId() {
        return mchId;
    }

    /**
     * 设置商户号
     *
     * @param mchId
     *            商户号
     */
    public final void setMchId(
            String mchId) {
        this.mchId = mchId;
    }

    /**
     * 返回 随机字符串
     *
     * @return 随机字符串
     */
    public final String getNonceStr() {
        return nonceStr;
    }

    /**
     * 设置随机字符串
     *
     * @param nonceStr
     *            随机字符串
     */
    public final void setNonceStr(
            String nonceStr) {
        this.nonceStr = nonceStr;
    }

    /**
     * 返回 签名
     *
     * @return 签名
     */
    public final String getSign() {
        return sign;
    }

    /**
     * 设置签名
     *
     * @param sign
     *            签名
     */
    public final void setSign(
            String sign) {
        this.sign = sign;
    }

    /**
     * 返回 微信订单号
     *
     * @return 微信订单号
     */
    public final String getTransactionId() {
        return transactionId;
    }

    /**
     * 设置微信订单号
     *
     * @param transactionId
     *            微信订单号
     */
    public final void setTransactionId(
            String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * 返回 商户订单号
     *
     * @return 商户订单号
     */
    public final String getOutTradeNo() {
        return outTradeNo;
    }

    /**
     * 设置商户订单号
     *
     * @param outTradeNo
     *            商户订单号
     */
    public final void setOutTradeNo(
            String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    /**
     * 返回 商户退款单号
     *
     * @return 商户退款单号
     */
    public final String getOutRefundNo() {
        return outRefundNo;
    }

    /**
     * 设置商户退款单号
     *
     * @param outRefundNo
     *            商户退款单号
     */
    public final void setOutRefundNo(
            String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    /**
     * 返回 微信退款单号
     *
     * @return 微信退款单号
     */
    public final String getRefundId() {
        return refundId;
    }

    /**
     * 设置微信退款单号
     *
     * @param refundId
     *            微信退款单号
     */
    public final void setRefundId(
            String refundId) {
        this.refundId = refundId;
    }

    /**
     * 返回 退款金额
     *
     * @return 退款金额
     */
    public final String getRefundFee() {
        return refundFee;
    }

    /**
     * 设置退款金额
     *
     * @param refundFee
     *            退款金额
     */
    public final void setRefundFee(
            String refundFee) {
        this.refundFee = refundFee;
    }

}
