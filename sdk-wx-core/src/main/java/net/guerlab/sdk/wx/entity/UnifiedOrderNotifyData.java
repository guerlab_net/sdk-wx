package net.guerlab.sdk.wx.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * 统一订单异步通知结果
 * 
 * @author guer
 *
 */
@ApiModel("统一订单异步通知结果")
public class UnifiedOrderNotifyData {

    private String appId;

    private String mchId;

    private String deviceInfo;

    private String nonceStr;

    private String sign;

    private String signType;

    private String openId;

    private boolean subscribe;

    private String tradeType;

    private String bankType;

    private BigDecimal totalFee;

    private BigDecimal settlementTotalFee;

    private String feeType;

    private BigDecimal cashFee;

    private String cashFeeType;

    private BigDecimal couponFee;

    private int couponCount;

    private List<Coupon> couponList = new ArrayList<>();

    private String transactionId;

    private String outTradeNo;

    private String attach;

    private LocalDateTime timeEnd;

    /**
     * 返回 appId
     *
     * @return appId
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 设置appId
     *
     * @param appId
     *            appId
     */
    public void setAppId(
            String appId) {
        this.appId = appId;
    }

    /**
     * 返回 mchId
     *
     * @return mchId
     */
    public String getMchId() {
        return mchId;
    }

    /**
     * 设置mchId
     *
     * @param mchId
     *            mchId
     */
    public void setMchId(
            String mchId) {
        this.mchId = mchId;
    }

    /**
     * 返回 deviceInfo
     *
     * @return deviceInfo
     */
    public String getDeviceInfo() {
        return deviceInfo;
    }

    /**
     * 设置deviceInfo
     *
     * @param deviceInfo
     *            deviceInfo
     */
    public void setDeviceInfo(
            String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    /**
     * 返回 nonceStr
     *
     * @return nonceStr
     */
    public String getNonceStr() {
        return nonceStr;
    }

    /**
     * 设置nonceStr
     *
     * @param nonceStr
     *            nonceStr
     */
    public void setNonceStr(
            String nonceStr) {
        this.nonceStr = nonceStr;
    }

    /**
     * 返回 sign
     *
     * @return sign
     */
    public String getSign() {
        return sign;
    }

    /**
     * 设置sign
     *
     * @param sign
     *            sign
     */
    public void setSign(
            String sign) {
        this.sign = sign;
    }

    /**
     * 返回 signType
     *
     * @return signType
     */
    public String getSignType() {
        return signType;
    }

    /**
     * 设置signType
     *
     * @param signType
     *            signType
     */
    public void setSignType(
            String signType) {
        this.signType = signType;
    }

    /**
     * 返回 openId
     *
     * @return openId
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * 设置openId
     *
     * @param openId
     *            openId
     */
    public void setOpenId(
            String openId) {
        this.openId = openId;
    }

    /**
     * 返回 subscribe
     *
     * @return subscribe
     */
    public boolean isSubscribe() {
        return subscribe;
    }

    /**
     * 设置subscribe
     *
     * @param subscribe
     *            subscribe
     */
    public void setSubscribe(
            boolean subscribe) {
        this.subscribe = subscribe;
    }

    /**
     * 返回 tradeType
     *
     * @return tradeType
     */
    public String getTradeType() {
        return tradeType;
    }

    /**
     * 设置tradeType
     *
     * @param tradeType
     *            tradeType
     */
    public void setTradeType(
            String tradeType) {
        this.tradeType = tradeType;
    }

    /**
     * 返回 bankType
     *
     * @return bankType
     */
    public String getBankType() {
        return bankType;
    }

    /**
     * 设置bankType
     *
     * @param bankType
     *            bankType
     */
    public void setBankType(
            String bankType) {
        this.bankType = bankType;
    }

    /**
     * 返回 totalFee
     *
     * @return totalFee
     */
    public BigDecimal getTotalFee() {
        return totalFee;
    }

    /**
     * 设置totalFee
     *
     * @param totalFee
     *            totalFee
     */
    public void setTotalFee(
            BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    /**
     * 返回 settlementTotalFee
     *
     * @return settlementTotalFee
     */
    public BigDecimal getSettlementTotalFee() {
        return settlementTotalFee;
    }

    /**
     * 设置settlementTotalFee
     *
     * @param settlementTotalFee
     *            settlementTotalFee
     */
    public void setSettlementTotalFee(
            BigDecimal settlementTotalFee) {
        this.settlementTotalFee = settlementTotalFee;
    }

    /**
     * 返回 feeType
     *
     * @return feeType
     */
    public String getFeeType() {
        return feeType;
    }

    /**
     * 设置feeType
     *
     * @param feeType
     *            feeType
     */
    public void setFeeType(
            String feeType) {
        this.feeType = feeType;
    }

    /**
     * 返回 cashFee
     *
     * @return cashFee
     */
    public BigDecimal getCashFee() {
        return cashFee;
    }

    /**
     * 设置cashFee
     *
     * @param cashFee
     *            cashFee
     */
    public void setCashFee(
            BigDecimal cashFee) {
        this.cashFee = cashFee;
    }

    /**
     * 返回 cashFeeType
     *
     * @return cashFeeType
     */
    public String getCashFeeType() {
        return cashFeeType;
    }

    /**
     * 设置cashFeeType
     *
     * @param cashFeeType
     *            cashFeeType
     */
    public void setCashFeeType(
            String cashFeeType) {
        this.cashFeeType = cashFeeType;
    }

    /**
     * 返回 couponFee
     *
     * @return couponFee
     */
    public BigDecimal getCouponFee() {
        return couponFee;
    }

    /**
     * 设置couponFee
     *
     * @param couponFee
     *            couponFee
     */
    public void setCouponFee(
            BigDecimal couponFee) {
        this.couponFee = couponFee;
    }

    /**
     * 返回 couponCount
     *
     * @return couponCount
     */
    public int getCouponCount() {
        return couponCount;
    }

    /**
     * 设置couponCount
     *
     * @param couponCount
     *            couponCount
     */
    public void setCouponCount(
            int couponCount) {
        this.couponCount = couponCount;
    }

    /**
     * 返回 couponList
     *
     * @return couponList
     */
    public List<Coupon> getCouponList() {
        return couponList;
    }

    /**
     * 设置couponList
     *
     * @param couponList
     *            couponList
     */
    public void setCouponList(
            List<Coupon> couponList) {
        this.couponList = couponList;
    }

    /**
     * 返回 transactionId
     *
     * @return transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * 设置transactionId
     *
     * @param transactionId
     *            transactionId
     */
    public void setTransactionId(
            String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * 返回 outTradeNo
     *
     * @return outTradeNo
     */
    public String getOutTradeNo() {
        return outTradeNo;
    }

    /**
     * 设置outTradeNo
     *
     * @param outTradeNo
     *            outTradeNo
     */
    public void setOutTradeNo(
            String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    /**
     * 返回 attach
     *
     * @return attach
     */
    public String getAttach() {
        return attach;
    }

    /**
     * 设置attach
     *
     * @param attach
     *            attach
     */
    public void setAttach(
            String attach) {
        this.attach = attach;
    }

    /**
     * 返回 timeEnd
     *
     * @return timeEnd
     */
    public LocalDateTime getTimeEnd() {
        return timeEnd;
    }

    /**
     * 设置timeEnd
     *
     * @param timeEnd
     *            timeEnd
     */
    public void setTimeEnd(
            LocalDateTime timeEnd) {
        this.timeEnd = timeEnd;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UnifiedOrderNotifyData [appId=");
        builder.append(appId);
        builder.append(", mchId=");
        builder.append(mchId);
        builder.append(", deviceInfo=");
        builder.append(deviceInfo);
        builder.append(", nonceStr=");
        builder.append(nonceStr);
        builder.append(", sign=");
        builder.append(sign);
        builder.append(", signType=");
        builder.append(signType);
        builder.append(", openId=");
        builder.append(openId);
        builder.append(", subscribe=");
        builder.append(subscribe);
        builder.append(", tradeType=");
        builder.append(tradeType);
        builder.append(", bankType=");
        builder.append(bankType);
        builder.append(", totalFee=");
        builder.append(totalFee);
        builder.append(", settlementTotalFee=");
        builder.append(settlementTotalFee);
        builder.append(", feeType=");
        builder.append(feeType);
        builder.append(", cashFee=");
        builder.append(cashFee);
        builder.append(", cashFeeType=");
        builder.append(cashFeeType);
        builder.append(", couponFee=");
        builder.append(couponFee);
        builder.append(", couponCount=");
        builder.append(couponCount);
        builder.append(", couponList=");
        builder.append(couponList);
        builder.append(", transactionId=");
        builder.append(transactionId);
        builder.append(", outTradeNo=");
        builder.append(outTradeNo);
        builder.append(", attach=");
        builder.append(attach);
        builder.append(", timeEnd=");
        builder.append(timeEnd);
        builder.append("]");
        return builder.toString();
    }

}
