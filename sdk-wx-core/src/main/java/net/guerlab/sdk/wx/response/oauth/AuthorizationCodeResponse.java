package net.guerlab.sdk.wx.response.oauth;

/**
 * AuthorizationCode响应
 * 
 * @author guer
 *
 */
public class AuthorizationCodeResponse extends AbstractOauthResponse<String> {

}
