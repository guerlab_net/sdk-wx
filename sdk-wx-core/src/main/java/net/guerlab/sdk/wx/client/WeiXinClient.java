package net.guerlab.sdk.wx.client;

import net.guerlab.sdk.wx.request.oauth.AbstractOauthRequest;
import net.guerlab.sdk.wx.request.pay.AbstractPayRequest;
import net.guerlab.sdk.wx.response.oauth.AbstractOauthResponse;
import net.guerlab.sdk.wx.response.pay.AbstractPayResponse;

/**
 * 微信请求客户端接口
 *
 * @author guer
 *
 */
public interface WeiXinClient {

    /**
     * 执行Oauth请求
     *
     * @param request
     *            Oauth请求
     * @param <T>
     *            响应类型
     * @param <E>
     *            响应数据类型
     * @return 响应
     */
    <T extends AbstractOauthResponse<E>, E> T execute(
            AbstractOauthRequest<T, E> request);

    /**
     * 执行支付请求
     *
     * @param request
     *            支付请求
     * @param <T>
     *            响应类型
     * @param <E>
     *            响应数据类型
     * @return 响应
     */
    <T extends AbstractPayResponse<E>, E> T execute(
            AbstractPayRequest<T, E> request);
}
