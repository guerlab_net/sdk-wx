package net.guerlab.sdk.wx.request.oauth;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.response.oauth.QrconnectResponse;

/**
 * Qrconnect请求
 * 
 * @author guer
 *
 */
public class QrconnectRequest extends AbstractOauthRequest<QrconnectResponse, String> {

    @Override
    protected StringBuilder createRequestUrl(
            String appId,
            String appKey,
            String payAppid,
            String paySecret,
            String payMchId,
            String payKey) {
        StringBuilder builder = new StringBuilder(WeiXinConstants.QRCONNECT_URL);
        builder.append("?response_type=code&appid=");
        builder.append(appId);
        return builder;
    }

    @Override
    public boolean needHttpRequest() {
        return false;
    }

    @Override
    protected void execut0(
            String responseData) {
        response = new QrconnectResponse();
        response.setData(responseData);
    }

    /**
     * 设置scope
     * 
     * @param scope
     *            scope
     */
    public void setScope(
            String scope) {
        requestParams.put("scope", scope);
    }

    /**
     * 获取scope
     * 
     * @return scope
     */
    public String getScope() {
        return requestParams.get("scope");
    }

    /**
     * 设置redirectUri
     * 
     * @param redirectUri
     *            redirectUri
     */
    public void setRedirectUri(
            String redirectUri) {
        requestParams.put("redirect_uri", redirectUri);
    }

    /**
     * 获取redirectUri
     * 
     * @return redirectUri
     */
    public String getRedirectUri() {
        return requestParams.get("redirect_uri");
    }

    /**
     * 设置state
     * 
     * @param state
     *            state
     */
    public void setState(
            String state) {
        requestParams.put("state", state);
    }

    /**
     * 获取state
     * 
     * @return state
     */
    public String getState() {
        return requestParams.get("state");
    }
}
