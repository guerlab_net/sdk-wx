package net.guerlab.sdk.wx.entity;

/**
 * 错误数据
 * 
 * @author guer
 *
 */
public class ErrorData {

    /**
     * 错误码
     */
    private String errcode;

    /**
     * 错误信息
     */
    private String errmsg;

    /**
     * 返回 错误码
     *
     * @return 错误码
     */
    public String getErrcode() {
        return errcode;
    }

    /**
     * 设置错误码
     *
     * @param errcode
     *            错误码
     */
    public void setErrcode(
            String errcode) {
        this.errcode = errcode;
    }

    /**
     * 返回 错误信息
     *
     * @return 错误信息
     */
    public String getErrmsg() {
        return errmsg;
    }

    /**
     * 设置错误信息
     *
     * @param errmsg
     *            错误信息
     */
    public void setErrmsg(
            String errmsg) {
        this.errmsg = errmsg;
    }
}
