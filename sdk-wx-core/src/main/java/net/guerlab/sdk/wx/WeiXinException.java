package net.guerlab.sdk.wx;

/**
 * 微信异常
 * 
 * @author guer
 *
 */
public class WeiXinException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 通过异常信息构造异常
     * 
     * @param message
     *            异常信息
     */
    public WeiXinException(
            String message) {
        super(message);
    }

    /**
     * 通过异常信息和源异常构造异常
     * 
     * @param message
     *            异常信息
     * @param e
     *            源异常
     */
    public WeiXinException(
            String message,
            Exception e) {
        super(message, e);
    }
}
