package net.guerlab.sdk.wx.response.pay;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 公众号支付参数
 *
 * @author guer
 *
 */
@ApiModel("公众号支付参数")
public class InnerH5Params {

    /**
     * 应用ID
     */
    @ApiModelProperty("应用ID")
    private String appId;

    /**
     * 时间戳
     */
    @ApiModelProperty("时间戳")
    private String timeStamp;

    /**
     * 随机串
     */
    @ApiModelProperty("随机串")
    private String nonceStr;

    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", name = "package")
    @JsonProperty("package")
    private String packages;

    /**
     * 微信签名方式
     */
    @ApiModelProperty("微信签名方式")
    private String signType;

    /**
     * 签名
     */
    @ApiModelProperty("签名")
    private String paySign;

    /**
     * 返回 应用ID
     *
     * @return 应用ID
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 设置应用ID
     *
     * @param appId
     *            应用ID
     */
    public void setAppId(
            String appId) {
        this.appId = appId;
    }

    /**
     * 返回 时间戳
     *
     * @return 时间戳
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置时间戳
     *
     * @param timeStamp
     *            时间戳
     */
    public void setTimeStamp(
            String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * 返回 随机串
     *
     * @return 随机串
     */
    public String getNonceStr() {
        return nonceStr;
    }

    /**
     * 设置随机串
     *
     * @param nonceStr
     *            随机串
     */
    public void setNonceStr(
            String nonceStr) {
        this.nonceStr = nonceStr;
    }

    /**
     * 返回 扩展字段
     *
     * @return 扩展字段
     */
    public String getPackages() {
        return packages;
    }

    /**
     * 设置扩展字段
     *
     * @param packages
     *            扩展字段
     */
    public void setPackages(
            String packages) {
        this.packages = packages;
    }

    /**
     * 返回 微信签名方式
     *
     * @return 微信签名方式
     */
    public String getSignType() {
        return signType;
    }

    /**
     * 设置微信签名方式
     *
     * @param signType
     *            微信签名方式
     */
    public void setSignType(
            String signType) {
        this.signType = signType;
    }

    /**
     * 返回 签名
     *
     * @return 签名
     */
    public String getPaySign() {
        return paySign;
    }

    /**
     * 设置签名
     *
     * @param paySign
     *            签名
     */
    public void setPaySign(
            String paySign) {
        this.paySign = paySign;
    }
}
