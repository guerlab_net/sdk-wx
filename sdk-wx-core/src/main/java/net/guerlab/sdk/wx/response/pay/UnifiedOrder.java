package net.guerlab.sdk.wx.response.pay;

/**
 * 统一支付订单
 * 
 * @author guer
 *
 */
public class UnifiedOrder {

    /**
     * 预支付交易会话标识
     */
    private String prepayId;

    /**
     * 二维码链接
     */
    private String codeUrl;

    /**
     * 支付跳转链接
     */
    private String mwebUrl;

    /**
     * 返回 预支付交易会话标识
     *
     * @return 预支付交易会话标识
     */
    public String getPrepayId() {
        return prepayId;
    }

    /**
     * 设置预支付交易会话标识
     *
     * @param prepayId
     *            预支付交易会话标识
     */
    public void setPrepayId(
            String prepayId) {
        this.prepayId = prepayId;
    }

    /**
     * 返回 二维码链接
     *
     * @return 二维码链接
     */
    public String getCodeUrl() {
        return codeUrl;
    }

    /**
     * 设置二维码链接
     *
     * @param codeUrl
     *            二维码链接
     */
    public void setCodeUrl(
            String codeUrl) {
        this.codeUrl = codeUrl;
    }

    /**
     * 返回 支付跳转链接
     *
     * @return 支付跳转链接
     */
    public String getMwebUrl() {
        return mwebUrl;
    }

    /**
     * 设置支付跳转链接
     *
     * @param mwebUrl
     *            支付跳转链接
     */
    public void setMwebUrl(
            String mwebUrl) {
        this.mwebUrl = mwebUrl;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codeUrl == null) ? 0 : codeUrl.hashCode());
        result = prime * result + ((mwebUrl == null) ? 0 : mwebUrl.hashCode());
        result = prime * result + ((prepayId == null) ? 0 : prepayId.hashCode());
        return result;
    }

    @Override
    public boolean equals(
            Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UnifiedOrder)) {
            return false;
        }
        UnifiedOrder other = (UnifiedOrder) obj;
        if (codeUrl == null) {
            if (other.codeUrl != null) {
                return false;
            }
        } else if (!codeUrl.equals(other.codeUrl)) {
            return false;
        }
        if (mwebUrl == null) {
            if (other.mwebUrl != null) {
                return false;
            }
        } else if (!mwebUrl.equals(other.mwebUrl)) {
            return false;
        }
        if (prepayId == null) {
            if (other.prepayId != null) {
                return false;
            }
        } else if (!prepayId.equals(other.prepayId)) {
            return false;
        }
        return true;
    }
}
