package net.guerlab.sdk.wx.request.pay;

import java.util.Map.Entry;

import net.guerlab.sdk.wx.helper.RandomStringHelper;
import net.guerlab.sdk.wx.response.pay.PayQrCodeResponse;

/**
 * 支付二维码
 *
 * @author guer
 *
 */
public class PayQrCodeRequest extends AbstractPayRequest<PayQrCodeResponse, String> {

    private String key;

    @Override
    public boolean needHttpRequest() {
        return false;
    }

    @Override
    protected StringBuilder createRequestUrl(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        key = payKey;
        requestParams.put("appid", payAppid);
        requestParams.put("mch_id", payMchId);
        requestParams.put("sign", getMD5Sign());

        StringBuilder builder = new StringBuilder("weixin://wxpay/bizpayurl?");

        return builder;
    }

    @Override
    protected void execut0(
            String responseData) {
        response = new PayQrCodeResponse();
        response.setData(responseData);
    }

    @Override
    protected String getStringA() {
        String nonceStr = RandomStringHelper.nextString(32);
        String timeStamp = Long.toString(System.currentTimeMillis() / 1000);

        requestParams.put("time_stamp", timeStamp);
        requestParams.put("nonce_str", nonceStr);

        StringBuilder builder = new StringBuilder();

        for (Entry<String, String> entry : requestParams.entrySet()) {
            builder.append(entry.getKey());
            builder.append("=");
            builder.append(entry.getValue());
            builder.append("&");
        }

        builder.append("key=");
        builder.append(key);

        return builder.toString();
    }

    /**
     * 设置商户ID
     *
     * @param productId
     *            商户ID
     */
    public void setProductId(
            String productId) {
        requestParams.put("product_id", productId);
    }

    /**
     * 返回商户ID
     *
     * @return 商户ID
     */
    public String getProductId() {
        return requestParams.get("product_id");
    }

}
