package net.guerlab.sdk.wx.request.pay;

import java.io.StringReader;
import java.util.Map.Entry;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.WeiXinException;
import net.guerlab.sdk.wx.helper.RandomStringHelper;
import net.guerlab.sdk.wx.response.pay.RefundParams;
import net.guerlab.sdk.wx.response.pay.RefundResponse;

/**
 * 退款请求
 *
 * @author guer
 *
 */
public class RefundRequest extends AbstractPayRequest<RefundResponse, RefundParams> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RefundRequest.class);

    private String key;

    @Override
    public String getMethod() {
        return WeiXinConstants.METHOD_POST;
    }

    @Override
    public String getFormat() {
        return WeiXinConstants.FORMAT_XML;
    }

    @Override
    public String getRequestContent() {
        requestParams.put("sign", getMD5Sign());

        StringBuilder xml = new StringBuilder("<xml>");

        for (Entry<String, String> entry : requestParams.entrySet()) {
            String paramsName = entry.getKey();
            xml.append("\n\t");
            xml.append("<" + paramsName + ">");
            xml.append(entry.getValue());
            xml.append("</" + paramsName + ">");
        }

        xml.append("\n</xml>");
        return xml.toString();
    }

    @Override
    public String getRequestUri(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        StringBuilder builder = createRequestUrl(payAppid, payKey, payMchId, paySecret);
        return builder.toString();
    }

    @Override
    protected StringBuilder createRequestUrl(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        key = payKey;
        requestParams.put("appId", payAppid);
        requestParams.put("mch_id", payMchId);

        return new StringBuilder(WeiXinConstants.REFUND_URL);
    }

    @Override
    protected void execut0(
            String responseData) {

        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(new StringReader(responseData));

            LOGGER.debug("document body :" + document.toString());

            Element root = document.getRootElement();

            String returnCode = root.elementTextTrim("return_code");
            String resultCode = root.elementTextTrim("result_code");

            if (WeiXinConstants.SUCCESS.equals(returnCode) && WeiXinConstants.SUCCESS.equals(resultCode)) {
                RefundParams refundParams = new RefundParams();
                refundParams.setAppId(root.elementTextTrim("appid"));
                refundParams.setMchId(root.elementTextTrim("mch_id"));
                refundParams.setNonceStr(root.elementTextTrim("nonce_str"));
                refundParams.setSign(root.elementTextTrim("sign"));
                refundParams.setTransactionId(root.elementTextTrim("transaction_id"));
                refundParams.setOutTradeNo(root.elementTextTrim("out_trade_no"));
                refundParams.setOutRefundNo(root.elementTextTrim("out_refund_no"));
                refundParams.setRefundId(root.elementTextTrim("refund_id"));
                refundParams.setRefundFee(root.elementTextTrim("refund_fee"));

                response = new RefundResponse();
                response.setData(refundParams);

            } else if (WeiXinConstants.FAIL.equals(returnCode)) {
                throw new WeiXinException(root.elementTextTrim("return_msg"));
            } else {
                throw new WeiXinException(
                        root.elementTextTrim("err_code") + ", " + root.elementTextTrim("err_code_des"));
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            throw new WeiXinException(e.getMessage(), e);
        }
    }

    @Override
    protected String getStringA() {
        String nonceStr = RandomStringHelper.nextString(32);

        requestParams.put("nonce_str", nonceStr);
        requestParams.put("sign_type", "MD5");

        StringBuilder builder = new StringBuilder();

        for (Entry<String, String> entry : requestParams.entrySet()) {
            builder.append(entry.getKey());
            builder.append("=");
            builder.append(entry.getValue());
            builder.append("&");
        }

        builder.append("key=");
        builder.append(key);

        return builder.toString();
    }

    /**
     * 返回 商户退款单号<br>
     * 商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
     *
     * @return 商户退款单号
     */
    public final String getOutRefundNo() {
        return requestParams.get("out_refund_no");
    }

    /**
     * 设置商户退款单号<br>
     * 商户系统内部的退款单号，商户系统内部唯一，只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔。
     *
     * @param outRefundNo
     *            商户退款单号
     */
    public final void setOutRefundNo(
            String outRefundNo) {
        requestParams.put("out_refund_no", outRefundNo);
    }

    /**
     * 返回 微信订单号<br>
     * 微信生成的订单号，在支付通知中有返回
     *
     * @return 微信订单号
     */
    public final String getTransactionId() {
        return requestParams.get("transaction_id");
    }

    /**
     * 设置微信订单号<br>
     * 微信生成的订单号，在支付通知中有返回
     *
     * @param transactionId
     *            微信订单号
     */
    public final void setTransactionId(
            String transactionId) {
        requestParams.put("transaction_id", transactionId);
    }

    /**
     * 返回 商户订单号<br>
     * 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
     *
     * @return 商户订单号
     */
    public final String getOutTradeNo() {
        return requestParams.get("out_trade_no");
    }

    /**
     * 设置商户订单号<br>
     * 商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
     *
     * @param outTradeNo
     *            商户订单号
     */
    public final void setOutTradeNo(
            String outTradeNo) {
        requestParams.put("out_trade_no", outTradeNo);
    }

    /**
     * 返回 订单金额<br>
     * 单位为分，只能为整数
     *
     * @return 订单金额
     */
    public final String getTotalFee() {
        return requestParams.get("total_fee");
    }

    /**
     * 设置订单金额<br>
     * 单位为分，只能为整数
     *
     * @param totalFee
     *            订单金额
     */
    public final void setTotalFee(
            String totalFee) {
        requestParams.put("total_fee", totalFee);
    }

    /**
     * 返回 退款金额<br>
     * 单位为分，只能为整数
     *
     * @return 退款金额
     */
    public final String getRefundFee() {
        return requestParams.get("refund_fee");
    }

    /**
     * 设置退款金额<br>
     * 单位为分，只能为整数
     *
     * @param refundFee
     *            退款金额
     */
    public final void setRefundFee(
            String refundFee) {
        requestParams.put("refund_fee", refundFee);
    }

    /**
     * 返回 货币种类<br>
     * 符合ISO 4217标准的三位字母代码，默认人民币：CNY
     *
     * @return 货币种类
     */
    public final String getRefundFeeType() {
        return requestParams.get("refund_fee_type");
    }

    /**
     * 设置货币种类<br>
     * 符合ISO 4217标准的三位字母代码，默认人民币：CNY
     *
     * @param refundFeeType
     *            货币种类
     */
    public final void setRefundFeeType(
            String refundFeeType) {
        requestParams.put("refund_fee_type", refundFeeType);
    }

    /**
     * 返回 退款原因<br>
     * 若商户传入，会在下发给用户的退款消息中体现退款原因
     *
     * @return 退款原因
     */
    public final String getRefundDesc() {
        return requestParams.get("refund_desc");
    }

    /**
     * 设置退款原因<br>
     * 若商户传入，会在下发给用户的退款消息中体现退款原因
     *
     * @param refundDesc
     *            退款原因
     */
    public final void setRefundDesc(
            String refundDesc) {
        requestParams.put("refund_desc", refundDesc);
    }

    /**
     * 返回 退款资金来源<br>
     * 仅针对老资金流商户使用<br>
     * REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）<br>
     * REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
     *
     * @return 退款资金来源
     */
    public final String getRefundAccount() {
        return requestParams.get("refund_account");
    }

    /**
     * 设置退款资金来源<br>
     * 仅针对老资金流商户使用<br>
     * REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）<br>
     * REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
     *
     * @param refundAccount
     *            退款资金来源
     */
    public final void setRefundAccount(
            String refundAccount) {
        requestParams.put("refund_account", refundAccount);
    }

}
