package net.guerlab.sdk.wx.response.pay;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP支付参数
 * 
 * @author guer
 *
 */
@ApiModel("APP支付参数")
public class AppPayParams {

    /**
     * 应用ID
     */
    @ApiModelProperty("应用ID")
    private String appId;

    /**
     * 商户号
     */
    @ApiModelProperty("商户号")
    private String partnerId;

    /**
     * 预支付交易会话ID
     */
    @ApiModelProperty("预支付交易会话ID")
    private String prepayId;

    /**
     * 扩展字段
     */
    @ApiModelProperty(value = "扩展字段", name = "package")
    @JsonProperty("package")
    private String packages;

    /**
     * 随机字符串
     */
    @ApiModelProperty("随机字符串")
    private String nonceStr;

    /**
     * 时间戳
     */
    @ApiModelProperty("时间戳")
    private String timeStamp;

    /**
     * 签名
     */
    @ApiModelProperty("签名")
    private String sign;

    /**
     * 返回 应用ID
     *
     * @return 应用ID
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 设置应用ID
     *
     * @param appId
     *            应用ID
     */
    public void setAppId(
            String appId) {
        this.appId = appId;
    }

    /**
     * 返回 商户号
     *
     * @return 商户号
     */
    public String getPartnerId() {
        return partnerId;
    }

    /**
     * 设置商户号
     *
     * @param partnerId
     *            商户号
     */
    public void setPartnerId(
            String partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * 返回 预支付交易会话ID
     *
     * @return 预支付交易会话ID
     */
    public String getPrepayId() {
        return prepayId;
    }

    /**
     * 设置预支付交易会话ID
     *
     * @param prepayId
     *            预支付交易会话ID
     */
    public void setPrepayId(
            String prepayId) {
        this.prepayId = prepayId;
    }

    /**
     * 返回 扩展字段
     *
     * @return 扩展字段
     */
    public String getPackages() {
        return packages;
    }

    /**
     * 设置扩展字段
     *
     * @param packages
     *            扩展字段
     */
    public void setPackages(
            String packages) {
        this.packages = packages;
    }

    /**
     * 返回 随机字符串
     *
     * @return 随机字符串
     */
    public String getNonceStr() {
        return nonceStr;
    }

    /**
     * 设置随机字符串
     *
     * @param nonceStr
     *            随机字符串
     */
    public void setNonceStr(
            String nonceStr) {
        this.nonceStr = nonceStr;
    }

    /**
     * 返回 时间戳
     *
     * @return 时间戳
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设置时间戳
     *
     * @param timeStamp
     *            时间戳
     */
    public void setTimeStamp(
            String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * 返回 签名
     *
     * @return 签名
     */
    public String getSign() {
        return sign;
    }

    /**
     * 设置签名
     *
     * @param sign
     *            签名
     */
    public void setSign(
            String sign) {
        this.sign = sign;
    }
}
