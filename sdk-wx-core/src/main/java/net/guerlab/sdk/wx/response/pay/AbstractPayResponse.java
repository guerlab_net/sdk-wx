package net.guerlab.sdk.wx.response.pay;

import net.guerlab.sdk.wx.response.AbstractResponse;

/**
 * 抽象支付响应
 * 
 * @author guer
 *
 * @param <T>
 *            响应数据类型
 */
public abstract class AbstractPayResponse<T> extends AbstractResponse<T> {
}
