package net.guerlab.sdk.wx.request.pay;

import java.util.Map.Entry;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.helper.RandomStringHelper;
import net.guerlab.sdk.wx.response.pay.AppPayParams;
import net.guerlab.sdk.wx.response.pay.AppPayResponse;

/**
 * 统一支付请求
 *
 * @author guer
 *
 */
public class AppPayRequest extends AbstractPayRequest<AppPayResponse, AppPayParams> {

    private String key;

    @Override
    public boolean needHttpRequest() {
        return false;
    }

    @Override
    protected StringBuilder createRequestUrl(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        key = payKey;
        requestParams.put("partnerid", payMchId);
        requestParams.put("appid", payAppid);
        requestParams.put("package", "Sign=WXPay");
        requestParams.put("sign", getMD5Sign());
        return new StringBuilder(WeiXinConstants.UNIFIEDORDER_URL);
    }

    @Override
    protected void execut0(
            String responseData) {
        AppPayParams appPayParams = new AppPayParams();
        appPayParams.setAppId(requestParams.get("appid"));
        appPayParams.setNonceStr(requestParams.get("noncestr"));
        appPayParams.setPackages(requestParams.get("package"));
        appPayParams.setPartnerId(requestParams.get("partnerid"));
        appPayParams.setPrepayId(requestParams.get("prepayid"));
        appPayParams.setTimeStamp(requestParams.get("timestamp"));
        appPayParams.setSign(requestParams.get("sign"));

        response = new AppPayResponse();
        response.setData(appPayParams);
    }

    @Override
    protected String getStringA() {
        String nonceStr = RandomStringHelper.nextString(32);
        String timeStamp = Long.toString(System.currentTimeMillis() / 1000);

        requestParams.put("timestamp", timeStamp);
        requestParams.put("noncestr", nonceStr);

        StringBuilder builder = new StringBuilder();

        for (Entry<String, String> entry : requestParams.entrySet()) {
            builder.append(entry.getKey());
            builder.append("=");
            builder.append(entry.getValue());
            builder.append("&");
        }

        builder.append("key=");
        builder.append(key);

        return builder.toString();
    }

    /**
     * 设置预支付ID
     *
     * @param prepayid
     *            预支付ID
     */
    public void setPrepayId(
            String prepayid) {
        requestParams.put("prepayid", prepayid);
    }

    /**
     * 获取预支付ID
     *
     * @return 预支付ID
     */
    public String getPrepayId() {
        return requestParams.get("prepayid");
    }

}
