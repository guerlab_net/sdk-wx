package net.guerlab.sdk.wx.request.pay;

import java.io.StringReader;
import java.util.Map.Entry;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.sdk.wx.WeiXinConstants;
import net.guerlab.sdk.wx.WeiXinException;
import net.guerlab.sdk.wx.helper.RandomStringHelper;
import net.guerlab.sdk.wx.response.pay.UnifiedOrder;
import net.guerlab.sdk.wx.response.pay.UnifiedOrderResponse;

/**
 * 统一支付请求
 *
 * @author guer
 *
 */
public class UnifiedOrderRequest extends AbstractPayRequest<UnifiedOrderResponse, UnifiedOrder> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnifiedOrderRequest.class);

    private String key;

    @Override
    public String getMethod() {
        return WeiXinConstants.METHOD_POST;
    }

    @Override
    public String getFormat() {
        return WeiXinConstants.FORMAT_XML;
    }

    @Override
    public String getRequestContent() {
        requestParams.put("sign", getMD5Sign());

        StringBuilder xml = new StringBuilder("<xml>");

        for (Entry<String, String> entry : requestParams.entrySet()) {
            String paramsName = entry.getKey();
            xml.append("\n\t");
            xml.append("<" + paramsName + ">");
            xml.append(entry.getValue());
            xml.append("</" + paramsName + ">");
        }

        xml.append("\n</xml>");
        return xml.toString();
    }

    @Override
    public String getRequestUri(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        StringBuilder builder = createRequestUrl(payAppid, payKey, payMchId, paySecret);
        return builder.toString();
    }

    @Override
    protected StringBuilder createRequestUrl(
            String payAppid,
            String payKey,
            String payMchId,
            String paySecret) {
        key = payKey;
        requestParams.put("mch_id", payMchId);
        requestParams.put("appid", payAppid);
        return new StringBuilder(WeiXinConstants.UNIFIEDORDER_URL);
    }

    @Override
    protected void execut0(
            String responseData) {

        String codeUrl = null;
        String prepayId = null;
        String mwebUrl = null;

        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(new StringReader(responseData));

            LOGGER.debug("document body :" + document.toString());

            Element root = document.getRootElement();

            String returnCode = root.elementTextTrim("return_code");
            String resultCode = root.elementTextTrim("result_code");

            if (WeiXinConstants.SUCCESS.equals(returnCode) && WeiXinConstants.SUCCESS.equals(resultCode)) {
                codeUrl = root.elementTextTrim("code_url");
                prepayId = root.elementTextTrim("prepay_id");
                mwebUrl = root.elementTextTrim("mweb_url");
            } else if (WeiXinConstants.FAIL.equals(returnCode)) {
                throw new WeiXinException(root.elementTextTrim("return_msg"));
            } else {
                throw new WeiXinException(
                        root.elementTextTrim("err_code") + ", " + root.elementTextTrim("err_code_des"));
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            throw new WeiXinException(e.getMessage(), e);
        }

        UnifiedOrder unifiedOrder = new UnifiedOrder();
        unifiedOrder.setCodeUrl(codeUrl);
        unifiedOrder.setPrepayId(prepayId);
        unifiedOrder.setMwebUrl(mwebUrl);

        response = new UnifiedOrderResponse();
        response.setData(unifiedOrder);
    }

    @Override
    protected String getStringA() {
        String nonceStr = RandomStringHelper.nextString(32);
        String timeStamp = Long.toString(System.currentTimeMillis() / 1000);

        requestParams.put("time_stamp", timeStamp);
        requestParams.put("nonce_str", nonceStr);

        StringBuilder builder = new StringBuilder();

        for (Entry<String, String> entry : requestParams.entrySet()) {
            builder.append(entry.getKey());
            builder.append("=");
            builder.append(entry.getValue());
            builder.append("&");
        }

        builder.append("key=");
        builder.append(key);

        return builder.toString();
    }

    /**
     * 获取body
     *
     * @return body
     */
    public String getBody() {
        return requestParams.get("body");
    }

    /**
     * 设置body
     *
     * @param body
     *            body
     */
    public void setBody(
            String body) {
        requestParams.put("body", body);
    }

    /**
     * 获取outTradeNo
     *
     * @return outTradeNo
     */
    public String getOutTradeNo() {
        return requestParams.get("out_trade_no");
    }

    /**
     * 设置outTradeNo
     *
     * @param outTradeNo
     *            outTradeNo
     */
    public void setOutTradeNo(
            String outTradeNo) {
        requestParams.put("out_trade_no", outTradeNo);
    }

    /**
     * 获取totalFee
     *
     * @return totalFee
     */
    public String getTotalFee() {
        return requestParams.get("total_fee");
    }

    /**
     * 设置totalFee
     *
     * @param totalFee
     *            totalFee
     */
    public void setTotalFee(
            String totalFee) {
        requestParams.put("total_fee", totalFee);
    }

    /**
     * 获取spbillCreateIp
     *
     * @return spbillCreateIp
     */
    public String getSpbillCreateIp() {
        return requestParams.get("spbill_create_ip");
    }

    /**
     * 设置spbillCreateIp
     *
     * @param spbillCreateIp
     *            spbillCreateIp
     */
    public void setSpbillCreateIp(
            String spbillCreateIp) {
        requestParams.put("spbill_create_ip", spbillCreateIp);
    }

    /**
     * 获取notifyUrl
     *
     * @return notifyUrl
     */
    public String getNotifyUrl() {
        return requestParams.get("notify_url");
    }

    /**
     * 设置notifyUrl
     *
     * @param notifyUrl
     *            notifyUrl
     */
    public void setNotifyUrl(
            String notifyUrl) {
        requestParams.put("notify_url", notifyUrl);
    }

    /**
     * 获取tradeType
     *
     * @return tradeType
     */
    public String getTradeType() {
        return requestParams.get("trade_type");
    }

    /**
     * 设置tradeType
     *
     * @param tradeType
     *            tradeType
     */
    public void setTradeType(
            String tradeType) {
        requestParams.put("trade_type", tradeType);
    }

    /**
     * 返回设备号
     *
     * @return 设备号
     */
    public String getDeviceInfo() {
        return requestParams.get("device_info");
    }

    /**
     * 设置设备号
     *
     * @param deviceInfo
     *            设备号
     */
    public void setDeviceInfo(
            String deviceInfo) {
        requestParams.put("device_info", deviceInfo);
    }

    /**
     * 返回商品详情
     *
     * @return 商品详情
     */
    public String getDetail() {
        return requestParams.get("detail");
    }

    /**
     * 设置商品详情
     *
     * @param detail
     *            商品详情
     */
    public void setDetail(
            String detail) {
        requestParams.put("detail", detail);
    }

    /**
     * 返回附加数据
     *
     * @return 附加数据
     */
    public String getAttach() {
        return requestParams.get("attach");
    }

    /**
     * 设置附加数据
     *
     * @param attach
     *            附加数据
     */
    public void setAttach(
            String attach) {
        requestParams.put("attach", attach);
    }

    /**
     * 返回标价币种
     *
     * @return 标价币种
     */
    public String getFeeType() {
        return requestParams.get("fee_type");
    }

    /**
     * 设置标价币种
     *
     * @param feeType
     *            标价币种
     */
    public void setFeeType(
            String feeType) {
        requestParams.put("fee_type", feeType);
    }

    /**
     * 返回交易起始时间
     *
     * @return 交易起始时间
     */
    public String getTimeStart() {
        return requestParams.get("time_start");
    }

    /**
     * 设置交易起始时间
     *
     * @param timeStart
     *            交易起始时间
     */
    public void setTimeStart(
            String timeStart) {
        requestParams.put("time_start", timeStart);
    }

    /**
     * 返回交易结束时间
     *
     * @return 交易结束时间
     */
    public String getTimeExpire() {
        return requestParams.get("time_expire");
    }

    /**
     * 设置交易结束时间
     *
     * @param timeExpire
     *            交易结束时间
     */
    public void setTimeExpire(
            String timeExpire) {
        requestParams.put("time_expire", timeExpire);
    }

    /**
     * 返回订单优惠标记
     *
     * @return 订单优惠标记
     */
    public String getGoodsTag() {
        return requestParams.get("goods_tag");
    }

    /**
     * 设置订单优惠标记
     *
     * @param goodsTag
     *            订单优惠标记
     */
    public void setGoodsTag(
            String goodsTag) {
        requestParams.put("goods_tag", goodsTag);
    }

    /**
     * 返回商品ID
     *
     * @return 商品ID
     */
    public String getProductId() {
        return requestParams.get("product_id");
    }

    /**
     * 设置商品ID
     *
     * @param productId
     *            商品ID
     */
    public void setProductId(
            String productId) {
        requestParams.put("product_id", productId);
    }

    /**
     * 返回指定支付方式
     *
     * @return 指定支付方式
     */
    public String getLimitPay() {
        return requestParams.get("limit_pay");
    }

    /**
     * 设置指定支付方式
     *
     * @param limitPay
     *            指定支付方式
     */
    public void setLimitPay(
            String limitPay) {
        requestParams.put("limit_pay", limitPay);
    }

    /**
     * 返回用户标识
     *
     * @return 用户标识
     */
    public String getOpenId() {
        return requestParams.get("openid");
    }

    /**
     * 设置用户标识
     *
     * @param openId
     *            用户标识
     */
    public void setOpenId(
            String openId) {
        requestParams.put("openid", openId);
    }

    /**
     * 返回场景信息
     *
     * @return 场景信息
     */
    public String getSceneInfo() {
        return requestParams.get("scene_info");
    }

    /**
     * 设置场景信息
     *
     * @param sceneInfo
     *            场景信息
     */
    public void setSceneInfo(
            String sceneInfo) {
        requestParams.put("scene_info", sceneInfo);
    }

}
