package net.guerlab.sdk.wx.entity;

import java.math.BigDecimal;

/**
 * 优惠券
 * 
 * @author guer
 *
 */
public class Coupon {

    /**
     * 优惠券ID
     */
    private String id;

    /**
     * 优惠券类型
     */
    private String type;

    /**
     * 优惠券金额
     */
    private BigDecimal fee;

    /**
     * 构造优惠券对象
     * 
     * @param id
     *            优惠券ID
     * @param type
     *            优惠券类型
     * @param fee
     *            优惠券金额
     */
    public Coupon(
            String id,
            String type,
            BigDecimal fee) {
        this.id = id;
        this.type = type;
        this.fee = fee;
    }

    /**
     * 返回 优惠券ID
     *
     * @return 优惠券ID
     */
    public String getId() {
        return id;
    }

    /**
     * 设置优惠券ID
     *
     * @param id
     *            优惠券ID
     */
    public void setId(
            String id) {
        this.id = id;
    }

    /**
     * 返回 优惠券类型
     *
     * @return 优惠券类型
     */
    public String getType() {
        return type;
    }

    /**
     * 设置优惠券类型
     *
     * @param type
     *            优惠券类型
     */
    public void setType(
            String type) {
        this.type = type;
    }

    /**
     * 返回 优惠券金额
     *
     * @return 优惠券金额
     */
    public BigDecimal getFee() {
        return fee;
    }

    /**
     * 设置优惠券金额
     *
     * @param fee
     *            优惠券金额
     */
    public void setFee(
            BigDecimal fee) {
        this.fee = fee;
    }
}
