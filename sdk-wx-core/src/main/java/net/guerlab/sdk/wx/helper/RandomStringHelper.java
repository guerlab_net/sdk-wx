package net.guerlab.sdk.wx.helper;

import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机字符串助手
 * 
 * @author guer
 *
 */
public class RandomStringHelper {

    /**
     * 默认随机字符集合
     */
    public static final char[] CHAR_LIST = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    /**
     * 自定义取值范围的随机字符串
     *
     * @param length
     *            字符串长度
     * @return 随机字符串
     */
    public static String nextString(
            final int length) {
        if (length <= 0) {
            return "";
        }

        char[] list = new char[length];

        for (int i = 0; i < list.length; i++) {
            list[i] = CHAR_LIST[ThreadLocalRandom.current().nextInt(CHAR_LIST.length)];
        }

        return new String(list);
    }

}
