package net.guerlab.sdk.wx.response.oauth;

/**
 * 用户信息响应
 * 
 * @author guer
 *
 */
public class UserinfoResponse extends AbstractOauthResponse<Userinfo> {

}
