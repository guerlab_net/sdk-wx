package net.guerlab.sdk.wx.response.oauth;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 用户信息
 * 
 * @author guer
 *
 */
public class Userinfo {

    /**
     * openid
     */
    @JsonProperty("openid")
    private String openId;

    /**
     * nickname
     */
    @JsonProperty("nickname")
    private String nickname;

    /**
     * sex
     */
    @JsonProperty("sex")
    private int sex;

    /**
     * province
     */
    @JsonProperty("province")
    private String province;

    /**
     * city
     */
    @JsonProperty("city")
    private String city;

    /**
     * country
     */
    @JsonProperty("country")
    private String country;

    /**
     * headimgurl
     */
    @JsonProperty("headimgurl")
    private String headimgurl;

    /**
     * privilege
     */
    @JsonProperty("privilege")
    private List<String> privilege;

    /**
     * unionid
     */
    @JsonProperty("unionid")
    private String unionId;

    /**
     * 返回 openId
     *
     * @return openId
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * 设置openId
     *
     * @param openId
     *            openId
     */
    public void setOpenId(
            String openId) {
        this.openId = openId;
    }

    /**
     * 返回 nickname
     *
     * @return nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * 设置nickname
     *
     * @param nickname
     *            nickname
     */
    public void setNickname(
            String nickname) {
        this.nickname = nickname;
    }

    /**
     * 返回 sex
     *
     * @return sex
     */
    public int getSex() {
        return sex;
    }

    /**
     * 设置sex
     *
     * @param sex
     *            sex
     */
    public void setSex(
            int sex) {
        this.sex = sex;
    }

    /**
     * 返回 province
     *
     * @return province
     */
    public String getProvince() {
        return province;
    }

    /**
     * 设置province
     *
     * @param province
     *            province
     */
    public void setProvince(
            String province) {
        this.province = province;
    }

    /**
     * 返回 city
     *
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置city
     *
     * @param city
     *            city
     */
    public void setCity(
            String city) {
        this.city = city;
    }

    /**
     * 返回 country
     *
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 设置country
     *
     * @param country
     *            country
     */
    public void setCountry(
            String country) {
        this.country = country;
    }

    /**
     * 返回 headimgurl
     *
     * @return headimgurl
     */
    public String getHeadimgurl() {
        return headimgurl;
    }

    /**
     * 设置headimgurl
     *
     * @param headimgurl
     *            headimgurl
     */
    public void setHeadimgurl(
            String headimgurl) {
        this.headimgurl = headimgurl;
    }

    /**
     * 返回 privilege
     *
     * @return privilege
     */
    public List<String> getPrivilege() {
        return privilege;
    }

    /**
     * 设置privilege
     *
     * @param privilege
     *            privilege
     */
    public void setPrivilege(
            List<String> privilege) {
        this.privilege = privilege;
    }

    /**
     * 返回 unionId
     *
     * @return unionId
     */
    public String getUnionId() {
        return unionId;
    }

    /**
     * 设置unionId
     *
     * @param unionId
     *            unionId
     */
    public void setUnionId(
            String unionId) {
        this.unionId = unionId;
    }

}
