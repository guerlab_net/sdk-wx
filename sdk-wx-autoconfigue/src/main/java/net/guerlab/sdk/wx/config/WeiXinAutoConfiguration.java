package net.guerlab.sdk.wx.config;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.guerlab.sdk.wx.client.WeiXinClient;
import net.guerlab.sdk.wx.client.impl.DefaultWeiXinClient;
import okhttp3.OkHttpClient;

/**
 *
 * @author guer
 *
 */
@Configuration
@RefreshScope
@EnableConfigurationProperties(WeiXinConfig.class)
public class WeiXinAutoConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeiXinAutoConfiguration.class);

    @Autowired
    private WeiXinConfig config;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired(required = false)
    private OkHttpClient okHttpClient;

    /**
     * 创建默认微信请求客户端
     *
     * @return 微信请求客户端
     */
    @RefreshScope
    @Bean
    public WeiXinClient weiXinClient() {
        String appId = config.getAppid();
        String secret = config.getSecret();
        String payAppid = config.getPayAppid();
        String paySecret = config.getPaySecret();
        String payMchId = config.getPayMchId();
        String payKey = config.getPayKey();

        return new DefaultWeiXinClient(appId, secret, payAppid, paySecret, payMchId, payKey, getOkHttpClient(),
                objectMapper);
    }

    private OkHttpClient getOkHttpClient() {
        if (okHttpClient != null) {
            return okHttpClient;
        }

        return createHttpClient();
    }

    /**
     * 创建http请求客户端
     *
     * @return http请求客户端
     */
    public static OkHttpClient createHttpClient() {
        return new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS).sslSocketFactory(createSSLSocketFactory(), new TrustAllManager())
                .hostnameVerifier((
                        hostname,
                        session) -> true)
                .build();
    }

    private static SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory sSLSocketFactory = null;

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[] {
                    new TrustAllManager()
            }, new SecureRandom());
            sSLSocketFactory = sc.getSocketFactory();
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
        }

        return sSLSocketFactory;
    }

    private static class TrustAllManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(
                X509Certificate[] chain,
                String authType) throws CertificateException {
            /**
             * not todo some thing
             */
        }

        @Override
        public void checkServerTrusted(
                X509Certificate[] chain,
                String authType) throws CertificateException {
            /**
             * not todo some thing
             */
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[] {};
        }
    }
}
