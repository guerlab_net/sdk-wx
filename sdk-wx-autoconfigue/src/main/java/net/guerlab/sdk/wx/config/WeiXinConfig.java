package net.guerlab.sdk.wx.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * 微信配置
 *
 * @author guer
 *
 */
@Component
@RefreshScope
@ConfigurationProperties(prefix = WeiXinConfig.CONFIG_PREFIX)
public class WeiXinConfig {

    public static final String CONFIG_PREFIX = "sdk.wx";

    /**
     * 应用ID
     */
    private String appid;

    /**
     * 应用密钥
     */
    private String secret;

    /**
     * 支付应用ID
     */
    private String payAppid;

    /**
     * 支付应用密钥
     */
    private String payKey;

    /**
     * 支付商户ID
     */
    private String payMchId;

    /**
     * 支付密钥
     */
    private String paySecret;

    /**
     * 回跳地址
     */
    private String redirectUri;

    /**
     * 请求范围
     */
    private String scope;

    /**
     * 返回页面前缀表
     */
    private Map<String, String> retrunUrls;

    /**
     * 返回 应用ID
     *
     * @return 应用ID
     */
    public String getAppid() {
        return appid;
    }

    /**
     * 设置应用ID
     *
     * @param appid
     *            应用ID
     */
    public void setAppid(
            String appid) {
        this.appid = appid;
    }

    /**
     * 返回 应用密钥
     *
     * @return 应用密钥
     */
    public String getSecret() {
        return secret;
    }

    /**
     * 设置应用密钥
     *
     * @param secret
     *            应用密钥
     */
    public void setSecret(
            String secret) {
        this.secret = secret;
    }

    /**
     * 返回 支付应用ID
     *
     * @return 支付应用ID
     */
    public String getPayAppid() {
        return payAppid;
    }

    /**
     * 设置支付应用ID
     *
     * @param payAppid
     *            支付应用ID
     */
    public void setPayAppid(
            String payAppid) {
        this.payAppid = payAppid;
    }

    /**
     * 返回 支付应用密钥
     *
     * @return 支付应用密钥
     */
    public String getPayKey() {
        return payKey;
    }

    /**
     * 设置支付应用密钥
     *
     * @param payKey
     *            支付应用密钥
     */
    public void setPayKey(
            String payKey) {
        this.payKey = payKey;
    }

    /**
     * 返回 支付商户ID
     *
     * @return 支付商户ID
     */
    public String getPayMchId() {
        return payMchId;
    }

    /**
     * 设置支付商户ID
     *
     * @param payMchId
     *            支付商户ID
     */
    public void setPayMchId(
            String payMchId) {
        this.payMchId = payMchId;
    }

    /**
     * 返回 支付密钥
     *
     * @return 支付密钥
     */
    public String getPaySecret() {
        return paySecret;
    }

    /**
     * 设置支付密钥
     *
     * @param paySecret
     *            支付密钥
     */
    public void setPaySecret(
            String paySecret) {
        this.paySecret = paySecret;
    }

    /**
     * 返回 回跳地址
     *
     * @return 回跳地址
     */
    public String getRedirectUri() {
        return redirectUri;
    }

    /**
     * 设置回跳地址
     *
     * @param redirectUri
     *            回跳地址
     */
    public void setRedirectUri(
            String redirectUri) {
        this.redirectUri = redirectUri;
    }

    /**
     * 返回 请求范围
     *
     * @return 请求范围
     */
    public String getScope() {
        return scope;
    }

    /**
     * 设置请求范围
     *
     * @param scope
     *            请求范围
     */
    public void setScope(
            String scope) {
        this.scope = scope;
    }

    /**
     * 返回 返回页面前缀表
     *
     * @return 返回页面前缀表
     */
    public Map<String, String> getRetrunUrls() {
        return retrunUrls;
    }

    /**
     * 设置返回页面前缀表
     *
     * @param retrunUrls
     *            返回页面前缀表
     */
    public void setRetrunUrls(
            Map<String, String> retrunUrls) {
        this.retrunUrls = retrunUrls;
    }

    /**
     * 获取返回页面前缀
     * 
     * @param type
     *            类型
     * @return 返回页面前缀
     */
    public String getRetrunUrl(
            String type) {
        return retrunUrls == null ? null : retrunUrls.get(type);
    }
}
